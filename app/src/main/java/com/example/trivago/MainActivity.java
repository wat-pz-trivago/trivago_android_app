package com.example.trivago;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.trivago.support_classes.JSONManager;
import com.example.trivago.support_classes.SharedPreferencesManager;
import com.example.trivago.ui.home.Home;

import org.json.JSONObject;

import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      //  getSupportActionBar().hide();

       // JSONManager jsonManager = new JSONManager();
        JSONObject initJSON = JSONManager.createJSON("Initial", null);

        // get json values for searching
        JSONManager.sendJSONObject(null, this.getApplicationContext(), "SearchFilters", null);

        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager();
     //   sharedPreferencesManager.clearUserData(this);

        sharedPreferencesManager.saveUserData(initJSON, getApplicationContext());

        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                startActivity(new Intent(MainActivity.this, Home.class));
                finish();
            }
        };

        handler.postDelayed(r, 3000);

    }



}


