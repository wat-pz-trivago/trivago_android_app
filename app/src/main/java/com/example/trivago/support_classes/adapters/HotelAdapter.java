package com.example.trivago.support_classes.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.trivago.R;
import com.example.trivago.support_classes.JSONManager;
import com.example.trivago.support_classes.filters.SearchFilters;
import com.example.trivago.support_classes.hotels.HotelContainer;
import com.example.trivago.ui.home.Home;
import com.example.trivago.ui.search_results.SearchResults;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;

public class HotelAdapter extends ArrayAdapter<HotelContainer> {

    private Context context;
    private Activity activity;
    private LinkedList<String> list = new LinkedList<String>();

    public HotelAdapter(Context context, ArrayList<HotelContainer> hotels, Activity activity) {
        super(context, 0, hotels);
        this.context = context;
        this.activity = activity;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final HotelContainer hotelContainer = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_hotel, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        TextView tvCountry = (TextView) convertView.findViewById(R.id.tvCountry);
        TextView tvCity = (TextView) convertView.findViewById(R.id.tvCity);
        TextView tvStreet = (TextView) convertView.findViewById(R.id.tvStreet);
        TextView tvHouseNumber = (TextView) convertView.findViewById(R.id.tvHouseNumber);
        TextView tvZipCode = (TextView) convertView.findViewById(R.id.tvZipCode);
        RatingBar rbStandard = (RatingBar) convertView.findViewById(R.id.rbHotelStandard);
      //  RatingBar tvStandard = (TextView) convertView.findViewById(R.id.tvStandard);
        TextView tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);
        Button bSelect = (Button) convertView.findViewById(R.id.bSelect);
        ImageView ivHotel = (ImageView) convertView.findViewById(R.id.ivHotel);
        // Populate the data into the template view using the data object
        tvName.setText(hotelContainer.getName());
        tvCountry.setText(hotelContainer.getCountry());
        tvCity.setText(hotelContainer.getCity());
        tvStreet.setText(hotelContainer.getStreet());
        tvHouseNumber.setText(hotelContainer.getHouseNumber());
        tvZipCode.setText(hotelContainer.getZipCode());
        rbStandard.setRating(Integer.parseInt(hotelContainer.getStandard()));
        tvDescription.setText(hotelContainer.getDescription());

        list.add(hotelContainer.getCountry());
        list.add(tvCity.getText().toString());
        list.add(tvName.getText().toString());
        list.add(SearchFilters.getFrom());
        list.add(SearchFilters.getTo());
        list.add(SearchFilters.getPersons());
        list.add("");

        System.out.println("Position: " + position);
        bSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRooms(position);
            }
        });

        // Return the completed view to render on screen
        return convertView;
    }

    public void getRooms(int position) {
        LinkedList<String> passList = new LinkedList<String>(list.subList(7 * position, 7 * position + 7));
        JSONObject json = JSONManager.createJSON("SendFilters", passList);
        JSONManager.sendJSONObject(null, context,"SendFilters", json);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // Actions to do after 10 seconds
                Home.redirectActivity(context, activity, SearchResults.class);
            }
        }, 1000);
    }
}
