package com.example.trivago.support_classes;

import android.content.Context;
import android.os.Build;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.trivago.support_classes.attractions.Attractions;
import com.example.trivago.support_classes.filters.SearchFilters;
import com.example.trivago.support_classes.hotels.Hotels;
import com.example.trivago.support_classes.reservations.Reservations;
import com.example.trivago.support_classes.rooms.Rooms;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

/**
 * Support class for creating and sending json objects to api
 */
public final class JSONManager {

    JSONObject returnObject;

    private JSONManager() {}

    /**
     * Function that creates JSON object
     * @param jsonObjectUsage "Initial", "Register", "SignIn", "SearchFilters", "SendFilters", "SendReservation", "EmailCheck", "GetAttractions", "GetAttractionDates"
     * @param sData Data that is turned into JSON object written in Strings
     * @return created JSONObject
     */
    public static JSONObject createJSON(String jsonObjectUsage, LinkedList<String> sData) {
        JSONObject json = new JSONObject();

        try {
            if(jsonObjectUsage.equals("Initial")) {
                JSONObject initialUser = new JSONObject();
                initialUser.put("BusinessRole", "Client");
                initialUser.put("FirstName", "");
                initialUser.put("LastName", "");
                initialUser.put("TelephoneNumber", "");
                initialUser.put("BirthDate", "");
                initialUser.put("Country", "");
                initialUser.put("City", "");
                initialUser.put("PostalCode", "");
                initialUser.put("Sex", "");

                json.put("Token", "");

                json.put("UserData", initialUser);
            }
            else if (jsonObjectUsage.equals("EmailCheck")) {
                json.put("Email", sData.get(0));
            }
            else if (jsonObjectUsage.equals("Register")) {

                json.put("Type", sData.get(0));
                json.put("Email", sData.get(1));
                json.put("Password", sData.get(2));
                json.put("FirstName", sData.get(3));
                json.put("LastName", sData.get(4));
                json.put("Sex", sData.get(5));
                json.put("BirthDate", sData.get(6));
                json.put("PhoneNumber", sData.get(7));
                json.put("Country", sData.get(8));
                json.put("City", sData.get(9));
                json.put("PostalCode", sData.get(10));

            }
            else if (jsonObjectUsage.equals("SignIn")) {

                json.put("Email", sData.get(0));
                json.put("Password", sData.get(1));

            }
            else if (jsonObjectUsage.equals("SendFilters")) {
                json.put("Country", sData.get(0));
                json.put("City", sData.get(1));
                json.put("Object", sData.get(2));
                json.put("From", sData.get(3));
                json.put("To", sData.get(4));

                json.put("Persons", sData.get(5));
                json.put("Standard", sData.get(6));
            }
            else if (jsonObjectUsage.equals("SendReservation")) {
                json.put("RoomId", sData.get(0));
                String sFrom = sData.get(1) + " " + "11:00";
                String sTo = sData.get(2) + " " + "10:00";
                json.put("From", sFrom);
                json.put("To", sTo);
            }
            else if (jsonObjectUsage.equals("GetAttractions")) {
                json.put("ReservationID", sData.get(0));
            }
            else if (jsonObjectUsage.equals("GetAttractionDates")) {
                json.put("AttractionId", sData.get(0));
                JSONArray array = new JSONArray();
                String from = sData.get(1).substring(0, 10);
                String to = sData.get(2).substring(0, 10);

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

                LinkedList<Date> datesInRange = new LinkedList<>();
                Calendar calendar = new GregorianCalendar();
                calendar.setTime(formatter.parse(from));

                Calendar endCalendar = new GregorianCalendar();
                endCalendar.setTime(formatter.parse(to));

                while (calendar.before(endCalendar)) {
                    Date result = calendar.getTime();
                    datesInRange.add(result);
                    calendar.add(Calendar.DATE, 1);
                }

                for(int i = 0; i < datesInRange.size(); i++) {
                    array.put(formatter.format(datesInRange.get(i)));
                }

                json.put("Dates", array);

                System.out.println(json.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * Function that sends created json object and receives response
     * @param context application context
     * @param jsonObjectUsage "Register", "SignIn", "SearchFilters", "SendFilters", "GetRoomReservations","GetAttractions", "GetAttractionDates"
     * @param json Created JSONObject using function createJSON
     * @return Response from api
     */
    public static void sendJSONObject(final TextView textView, final Context context, final String jsonObjectUsage, JSONObject json) {
        String sURL = null;
        int iMethodType = 0;
        if(jsonObjectUsage.equals("Register") || jsonObjectUsage.equals("EmailCheck")) {
            sURL = "https://7f6kavfkr2.execute-api.eu-west-1.amazonaws.com/v1/register";
            iMethodType = Request.Method.POST;
        }
        else if (jsonObjectUsage.equals("SignIn")) {
            try{
                sURL = "https://7f6kavfkr2.execute-api.eu-west-1.amazonaws.com/v1/login?email="
                        + json.getString("Email") + "&password=" + json.getString("Password");
                json = null;
            } catch (Exception e) {
                e.printStackTrace();
            }

            iMethodType = Request.Method.GET;
        }
        else if (jsonObjectUsage.equals("SearchFilters")) {
            try{
                sURL = "https://7f6kavfkr2.execute-api.eu-west-1.amazonaws.com/v1/search-engine";
            } catch (Exception e) {
                e.printStackTrace();
            }

            iMethodType = Request.Method.GET;
        }
        else if (jsonObjectUsage.equals("SendFilters")) {
            try{
                if(json.getString("Country").equals("All")) {// all filters set to all

                    if(json.getString("Standard").equals("")) {
                        sURL = "https://7f6kavfkr2.execute-api.eu-west-1.amazonaws.com/v1/search-engine?" + "persons=" + json.getString("Persons");

                    } else {
                        sURL = "https://7f6kavfkr2.execute-api.eu-west-1.amazonaws.com/v1/search-engine?" + "persons=" + json.getString("Persons")
                        + "&standard=" + json.getString("Standard");
                    }

                    json.remove("Object");
                } // filters city and hotel are set to all
                else if(!json.getString("Country").equals("All") && json.getString("City").equals("All")) {

                    if(json.getString("Standard").equals("")) {
                        sURL = "https://7f6kavfkr2.execute-api.eu-west-1.amazonaws.com/v1/search-engine?" + "persons=" + json.getString("Persons")
                        + "&country=" + json.getString("Country");

                    } else {
                        sURL = "https://7f6kavfkr2.execute-api.eu-west-1.amazonaws.com/v1/search-engine?" + "persons=" + json.getString("Persons")
                                + "&standard=" + json.getString("Standard") + "&country=" + json.getString("Country");
                    }

                    json.remove("Object");
                }// only hotel filter is set to all
                else if(!json.getString("Country").equals("All") && !json.getString("City").equals("All") && json.getString("Object").equals("All")) {

                    if(json.getString("Standard").equals("")) {
                        sURL = "https://7f6kavfkr2.execute-api.eu-west-1.amazonaws.com/v1/search-engine?" + "persons=" + json.getString("Persons")
                                + "&country=" + json.getString("Country") + "&city=" + json.getString("City");
                    } else {
                        sURL = "https://7f6kavfkr2.execute-api.eu-west-1.amazonaws.com/v1/search-engine?" + "persons=" + json.getString("Persons")
                                + "&standard=" + json.getString("Standard") + "&country=" + json.getString("Country") + "&city=" + json.getString("City");
                    }

                    json.remove("Object");
                }
                else {
                    sURL = "https://7f6kavfkr2.execute-api.eu-west-1.amazonaws.com/v1/search-engine?" + "persons=" + json.getString("Persons")
                            + "&country=" + json.getString("Country") + "&city=" + json.getString("City");
                }

                json.remove("Country");
                json.remove("City");
                json.remove("Persons");
                json.remove("Standard");
            } catch (Exception e) {
                e.printStackTrace();
            }

            iMethodType = Request.Method.POST;
        }
        else if (jsonObjectUsage.equals("SendReservation")) {
            sURL = "https://7f6kavfkr2.execute-api.eu-west-1.amazonaws.com/v1/reservations";
            iMethodType = Request.Method.POST;
        }
        else if (jsonObjectUsage.equals("GetRoomReservations")) {
            sURL = "https://7f6kavfkr2.execute-api.eu-west-1.amazonaws.com/v1/reservations/rooms?Email_Address=" + SharedPreferencesManager.getEmail(context);
            iMethodType = Request.Method.GET;
        }
        else if (jsonObjectUsage.equals("GetAttractions")) {
            try {
                sURL = "https://7f6kavfkr2.execute-api.eu-west-1.amazonaws.com/v1/search-engine/attractions?reservation_id=" + json.get("ReservationID").toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            iMethodType = Request.Method.GET;
            json = null;
        }
        else if (jsonObjectUsage.equals("GetAttractionDates")) {
            sURL = "https://7f6kavfkr2.execute-api.eu-west-1.amazonaws.com/v1/search-engine/attractions";
            iMethodType = Request.Method.POST;
        }

        Messages.saveMessage(null, jsonObjectUsage, context);

    //    textView.setText(json.toString());
        if(SharedPreferencesManager.getToken(context).equals("")) {
            userNotLogged(iMethodType, sURL, textView, context, jsonObjectUsage, json);
        } else {
            userLogged(iMethodType, sURL, textView, context, jsonObjectUsage, json);
        }

  //      return returnObject;
    }

    /**
     * function sends json when user is logged, using token authorization
     * @param iMethodType
     * @param sURL
     * @param textView
     * @param context
     * @param jsonObjectUsage
     * @param json
     */
    public static void userLogged(final int iMethodType, final String sURL, final TextView textView, final Context context, final String jsonObjectUsage, final JSONObject json) {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (iMethodType, sURL, json, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //     textView.setText("Response: " + response.toString());
                        if(jsonObjectUsage.equals("SignIn")){
                            SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager();
                            sharedPreferencesManager.saveUserData(response, context);
                        }
                        else if (jsonObjectUsage.equals("SearchFilters")) {

                            SearchFilters.saveFilters(response);

                        }
                        else if (jsonObjectUsage.equals("SendFilters")) {
                            if(response.has("Objects")) {
                                Hotels.saveHotels(response);
                                SearchFilters.setbAttraction(false);
                                SearchFilters.setbHotel(true);
                                SearchFilters.setbRoom(false);
                            } else {
                                Rooms.saveRooms(response);

                            }

                        }
                        else if (jsonObjectUsage.equals("GetRoomReservations")) {
                            Reservations.saveAllRoomReservations(response);
                        }
                        else if (jsonObjectUsage.equals("GetAttractions")) {
                            Attractions.saveAllAttractions(response);
                            SearchFilters.setbAttraction(true);
                            SearchFilters.setbHotel(false);
                            SearchFilters.setbRoom(false);
                        }
                        else if (jsonObjectUsage.equals("GetAttractionDates")) {

                        }

                        if(textView != null) { textView.setText(response.toString()); }

                        Messages.saveMessage(response, jsonObjectUsage, context);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        if(textView != null) { textView.setText("Error: " + error.toString()); }

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            Messages.saveMessage(data, jsonObjectUsage, context);
                        } catch (Exception e) {

                        }

                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                String sToken = "Bearer " + SharedPreferencesManager.getToken(context).toString();
                headers.put("Authorization", sToken);
                return headers;
            }
        };
        Volley.newRequestQueue(context).add(jsonObjectRequest);
    }

    /**
     * function sends json when user is not logged, no authorization
     * @param iMethodType
     * @param sURL
     * @param textView
     * @param context
     * @param jsonObjectUsage
     * @param json
     */
    public static void userNotLogged(final int iMethodType, final String sURL, final TextView textView, final Context context, final String jsonObjectUsage, final JSONObject json) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (iMethodType, sURL, json, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //     textView.setText("Response: " + response.toString());
                        if(jsonObjectUsage.equals("SignIn")){
                            SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager();
                            sharedPreferencesManager.saveUserData(response, context);
                        }
                        else if (jsonObjectUsage.equals("SearchFilters")) {
                            SearchFilters.saveFilters(response);
                        }
                        else if (jsonObjectUsage.equals("SendFilters")) {
                            if(response.has("Objects")) {
                                Hotels.saveHotels(response);
                                SearchFilters.setbAttraction(false);
                                SearchFilters.setbHotel(true);
                                SearchFilters.setbRoom(false);
                            } else {
                                Rooms.saveRooms(response);
                                SearchFilters.setbAttraction(false);
                                SearchFilters.setbHotel(false);
                                SearchFilters.setbRoom(true);
                            }
                        }

                        if(textView != null) { textView.setText(response.toString()); }

                        Messages.saveMessage(response, jsonObjectUsage, context);

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        if(textView != null) { textView.setText("Error: " + error.toString()); }
                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            Messages.saveMessage(data, jsonObjectUsage, context);
                        } catch (Exception e) {

                        }
                    }
                });
        Volley.newRequestQueue(context).add(jsonObjectRequest);
    }
}
