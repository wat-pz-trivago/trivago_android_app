package com.example.trivago.support_classes.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.example.trivago.R;
import com.example.trivago.support_classes.JSONManager;
import com.example.trivago.support_classes.Messages;
import com.example.trivago.support_classes.filters.SearchFilters;
import com.example.trivago.support_classes.SharedPreferencesManager;
import com.example.trivago.support_classes.rooms.RoomContainer;
import com.example.trivago.support_classes.rooms.Rooms;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;

public class RoomAdapter extends ArrayAdapter<RoomContainer> {

    private Context context;
    private Activity activity;
    private LinkedList<String> list = new LinkedList<String>();

    public RoomAdapter(Context context, ArrayList<RoomContainer> rooms, Activity activity) {
        super(context, 0, rooms);
        this.context = context;
        this.activity = activity;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        RoomContainer roomContainer = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_room, parent, false);
        }
        // Lookup view for data population
        TextView tvRoomNumber = (TextView) convertView.findViewById(R.id.tvRoomNumber);
        TextView tvPrice = (TextView) convertView.findViewById(R.id.tvPrice);
        TextView tvBedConfiguration = (TextView) convertView.findViewById(R.id.tvBedConfiguration);
        TextView tvGuestsAmount = (TextView) convertView.findViewById(R.id.tvGuestsAmount);
        TextView tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);
        Button bReserve = (Button) convertView.findViewById(R.id.bReserve);

        // Populate the data into the template view using the data object
        tvRoomNumber.setText(roomContainer.getRoomNumber());
        tvPrice.setText(roomContainer.getPrice());
        tvBedConfiguration.setText(roomContainer.getBedConfiguration());
        tvGuestsAmount.setText(roomContainer.getGuestsAmount());
        tvDescription.setText(roomContainer.getDescription());

        list.add(Rooms.getRoom(position).getRoomId());
        list.add(SearchFilters.getFrom());
        list.add(SearchFilters.getTo());

        bReserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendReservation();
            }
        });

        // Return the completed view to render on screen
        return convertView;
    }

    public void sendReservation() {
        JSONObject json = JSONManager.createJSON("SendReservation", list);
        JSONManager.sendJSONObject(null, context, "SendReservation", json);
        System.out.println(json.toString());
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // Actions to do after 1 second

                AlertDialog alertDialog = new AlertDialog.Builder(context).create();

                if(SharedPreferencesManager.getToken(context).equals("")) {
                    alertDialog.setMessage("Please sign in to place your reservation");
                } else {
                    if(Messages.getMessage().equals("Good parameters")) {
                        alertDialog.setMessage("Reservation send. Check your email.");
                    } else {
                        alertDialog.setMessage(Messages.getMessage());

                    }
                }

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        }, 1000);

    }
}
