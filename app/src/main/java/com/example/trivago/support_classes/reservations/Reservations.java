package com.example.trivago.support_classes.reservations;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;

/**
 * class storing user reservations
 */
public final class Reservations {

    private static LinkedList<RoomReservationContainer> roomReservation = new LinkedList<RoomReservationContainer>();
    private static LinkedList<AttractionReservationContainer> attractionReservation = new LinkedList<AttractionReservationContainer>();

    private Reservations() {}

    public static LinkedList<RoomReservationContainer> getAllReservations(){
        return roomReservation;
    }

    public static RoomReservationContainer getReservation(int index) {
        return roomReservation.get(index);
    }

    public static void saveAllRoomReservations(JSONObject json) {
        roomReservation.clear();

        try {
            JSONArray objects = json.getJSONArray("Reservations");
            for(int i = 0; i < objects.length(); i++) {
                JSONObject currentObject = objects.getJSONObject(i);

                JSONObject currentReservation = currentObject.getJSONObject("Reservation");
                JSONObject currentHotel = currentObject.getJSONObject("Object");
                JSONObject currentRoom = currentObject.getJSONObject("Room");
                RoomReservationContainer roomReservationContainer = new RoomReservationContainer();
                roomReservationContainer.setObjectID(currentHotel.get("ObjectId").toString());
                roomReservationContainer.setFrom(currentReservation.get("From").toString());
                roomReservationContainer.setTo(currentReservation.get("To").toString());
                roomReservationContainer.setStatus(currentReservation.get("ReservationStatus").toString());
                roomReservationContainer.setRoomID(currentReservation.get("RoomId").toString());
                roomReservationContainer.setObjectName(currentHotel.get("ObjectName").toString());
                roomReservationContainer.setTotalPrice(currentReservation.get("TotalCost").toString());
                roomReservationContainer.setUnitPrice(currentRoom.get("Price").toString());
                roomReservationContainer.setDescription(currentRoom.get("Description").toString());
                roomReservationContainer.setReservationID(currentReservation.get("ReservationId").toString());
                roomReservation.add(roomReservationContainer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
