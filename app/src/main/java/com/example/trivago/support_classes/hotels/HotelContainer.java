package com.example.trivago.support_classes.hotels;

import java.util.LinkedList;

/**
 * storing values for one hotel
 */
public class HotelContainer {

    private String objectId;
    private String name;
    private String country;
    private String city;
    private String street;
    private String houseNumber;
    private String zipCode;
    private String standard;
    private String description;

    public HotelContainer() {
        objectId = null;
        name = null;
        country = null;
        city = null;
        street = null;
        houseNumber = null;
        zipCode = null;
        standard = null;
        description = null;
    };

    public String getObjectId() {
        return objectId;
    }


    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
