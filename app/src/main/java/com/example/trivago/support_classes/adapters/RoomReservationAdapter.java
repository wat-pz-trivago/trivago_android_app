package com.example.trivago.support_classes.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.trivago.R;
import com.example.trivago.support_classes.JSONManager;
import com.example.trivago.support_classes.filters.ReservationFilters;
import com.example.trivago.support_classes.filters.SearchFilters;
import com.example.trivago.support_classes.reservations.RoomReservationContainer;
import com.example.trivago.ui.home.Home;
import com.example.trivago.ui.search_results.SearchResults;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class RoomReservationAdapter extends ArrayAdapter<RoomReservationContainer> {

    private Context context;
    private Activity activity;
    private LinkedList<String> list = new LinkedList<String>();

    public RoomReservationAdapter(Context context, ArrayList<RoomReservationContainer> reservations, Activity activity) {
        super(context, 0, reservations);
        this.context = context;
        this.activity = activity;
    };

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final RoomReservationContainer roomReservationContainer = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_room_reservation, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.tvReservationName);
        final TextView tvDateFrom = (TextView) convertView.findViewById(R.id.tvReservationDateFrom);
        final TextView tvDateTo = (TextView) convertView.findViewById(R.id.tvReservationDateTo);
        TextView tvStatus = (TextView) convertView.findViewById(R.id.tvReservationStatus);
        TextView tvUnitPrice = (TextView) convertView.findViewById(R.id.tvReservationUnitPrice);
        TextView tvTotalPrice = (TextView) convertView.findViewById(R.id.tvReservationTotalPrice);
        final TextView tvRoomReservationID = (TextView) convertView.findViewById(R.id.tvRoomReservationID);
        Button bResign = (Button) convertView.findViewById(R.id.bReservationResign);
        Button bSearch = (Button) convertView.findViewById(R.id.bSearchAttractions);
        // Populate the data into the template view using the data object
        tvName.setText(roomReservationContainer.getObjectName());
        tvDateFrom.setText(roomReservationContainer.getFrom());
        tvDateTo.setText(roomReservationContainer.getTo());
        tvStatus.setText(roomReservationContainer.getStatus());
        tvUnitPrice.setText(roomReservationContainer.getUnitPrice());
        tvTotalPrice.setText(roomReservationContainer.getTotalPrice());
        tvRoomReservationID.setText(roomReservationContainer.getReservationID());

        list.add(tvRoomReservationID.getText().toString());



        // Return the completed view to render on screen
        bSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAttractions(position, tvDateFrom, tvDateTo, tvRoomReservationID);
            }
        });

        return convertView;
    }

    public void getAttractions(int position, final TextView tvDateFrom, final TextView tvDateTo, final TextView tvRoomReservationID) {
        LinkedList<String> passList = new LinkedList<String>(Collections.singleton(list.get(position)));
        JSONObject json = JSONManager.createJSON("GetAttractions", passList);
        JSONManager.sendJSONObject(null, context, "GetAttractions", json);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // Actions to do after 10 seconds
                SearchFilters.setbAttraction(true);
                SearchFilters.setbHotel(false);
                SearchFilters.setbRoom(false);
                ReservationFilters.setFrom(tvDateFrom.getText().toString());
                ReservationFilters.setTo(tvDateTo.getText().toString());
                ReservationFilters.setReservationId(tvRoomReservationID.getText().toString());
                Home.redirectActivity(context, activity, SearchResults.class);
            }
        }, 3000);

    }

}
