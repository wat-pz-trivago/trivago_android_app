package com.example.trivago.support_classes.filters;

/**
 * class contains values of selected reservation from reservations list
 */
public final class ReservationFilters {

    private static String reservationId;
    private static String from;
    private static String to;

    private ReservationFilters() {}

    public static String getReservationId() {
        return reservationId;
    }

    public static void setReservationId(String reservationId) {
        ReservationFilters.reservationId = reservationId;
    }

    public static String getFrom() {
        return from;
    }

    public static void setFrom(String from) {
        ReservationFilters.from = from;
    }

    public static String getTo() {
        return to;
    }

    public static void setTo(String to) {
        ReservationFilters.to = to;
    }


}
