package com.example.trivago.support_classes;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONObject;

/**
 * Class used to store and retrieve data from shared preferences file
 */
public final class SharedPreferencesManager {

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    String sCity, sSex, sPhoneNumber, sBirthDate, sFirstName, sBusinessRole;
    String sLastName, sPostalCode, sCountry;

    public SharedPreferencesManager() {}

    /**
     * Function used to clear signed user data
     * @param context application/activity context
     */
    public static void clearUserData(Context context) {
        sharedPreferences = context.getSharedPreferences("UserData", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    /**
     * Function used to save user data after successful sign in
     * @param json  JSON received after successful sign in
     * @param context  application/activity context
     */
    public void saveUserData(JSONObject json, Context context) {

        clearUserData(context);

        try {

            JSONObject userData = json.getJSONObject("UserData");
            // Get user data from json
            sBusinessRole = userData.getString("BusinessRole");
            sFirstName = userData.getString("FirstName");
            sLastName = userData.getString("LastName");
            sPhoneNumber = userData.getString("TelephoneNumber");
            sBirthDate = userData.getString("BirthDate");
            sCountry = userData.getString("Country");
            sCity = userData.getString("City");
            sPostalCode = userData.getString("PostalCode");
            sSex = userData.getString("Sex");

            // Get token from json
            String sToken = json.getString("Token");

            // Get shared preferences for app
            sharedPreferences = context.getSharedPreferences("UserData", Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();

            // Save token to file
            editor.putString("Token", sToken);

            // Save user data to file
            editor.putString("FirstName", sFirstName);
            editor.putString("LastName", sLastName);
            editor.putString("PhoneNumber", sPhoneNumber);
            editor.putString("BirthDate", sBirthDate);
            editor.putString("Country", sCountry);
            editor.putString("City", sCity);
            editor.putString("PostalCode", sPostalCode);
            editor.putString("Sex", sSex);
            editor.putString("BusinessRole", sBusinessRole);

            // Save changes to file
            editor.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Function retrieves signed user token
     * @param context application context
     * @return user JSON web token
     */
    public static String getToken(Context context) {
        sharedPreferences = context.getSharedPreferences("UserData", context.MODE_PRIVATE);
        return sharedPreferences.getString("Token", "");
    }

    /**
     * Function retrieves signed user firstname
     * @param context application context
     * @return User account data
     */
    public String getFirstName(Context context) {
        sharedPreferences = context.getSharedPreferences("UserData", context.MODE_PRIVATE);
        return sharedPreferences.getString("FirstName", "");
    }

    /**
     * Function retrieves signed user last name
     * @param context application context
     * @return User account data
     */
    public String getLastName(Context context) {
        sharedPreferences = context.getSharedPreferences("UserData", context.MODE_PRIVATE);
        return sharedPreferences.getString("LastName", "");
    }

    /**
     * Function retrieves signed user phone number
     * @param context application context
     * @return User account data
     */
    public String getPhoneNumber(Context context) {
        sharedPreferences = context.getSharedPreferences("UserData", context.MODE_PRIVATE);
        return sharedPreferences.getString("PhoneNumber", "");
    }

    /**
     * Function retrieves signed user Birthdate
     * @param context application context
     * @return User account data
     */
    public String getBirthDate(Context context) {
        sharedPreferences = context.getSharedPreferences("UserData", context.MODE_PRIVATE);
        return sharedPreferences.getString("BirthDate", "");
    }

    /**
     * Function retrieves signed user Country
     * @param context application context
     * @return User account data
     */
    public String getCountry(Context context) {
        sharedPreferences = context.getSharedPreferences("UserData", context.MODE_PRIVATE);
        return sharedPreferences.getString("Country", "");
    }

    /**
     * Function retrieves signed user city
     * @param context application context
     * @return User account data
     */
    public String getCity(Context context) {
        sharedPreferences = context.getSharedPreferences("UserData", context.MODE_PRIVATE);
        return sharedPreferences.getString("City", "");
    }

    /**
     * Function retrieves signed user postal code
     * @param context application context
     * @return User account data
     */
    public String getPostalCode(Context context) {
        sharedPreferences = context.getSharedPreferences("UserData", context.MODE_PRIVATE);
        return sharedPreferences.getString("PostalCode", "");
    }

    /**
     * Function retrieves signed user sex
     * @param context application context
     * @return User account data
     */
    public String getSex(Context context) {
        sharedPreferences = context.getSharedPreferences("UserData", context.MODE_PRIVATE);
        return sharedPreferences.getString("Sex", "");
    }

    /**
     * Function retrieves signed user business role
     * @param context application context
     * @return User account data
     */
    public static String getBusinessRole(Context context) {
        sharedPreferences = context.getSharedPreferences("UserData", context.MODE_PRIVATE);
        return sharedPreferences.getString("BusinessRole", "");
    }

    /**
     * Function retrieves signed user email address
     * @param context application context
     * @return User account data
     */
    public static String getEmail(Context context) {
        sharedPreferences = context.getSharedPreferences("UserEmail", context.MODE_PRIVATE);
        return sharedPreferences.getString("Email", "");
    }

    /**
     * function saves email to shared preferences
     * @param context application context
     * @param sEmail User account data
     */
    public static void saveEmail(Context context, String sEmail) {
        // Get shared preferences for app
        sharedPreferences = context.getSharedPreferences("UserEmail", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        editor.putString("Email", sEmail);
        editor.commit();
    }

    /**
     * function saves password to shared preferences
     * @param context application context
     * @param sPassword User account data
     */
    public static void savePassword(Context context, String sPassword) {
        sharedPreferences = context.getSharedPreferences("UserPassword", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        editor.putString("Password", sPassword);
        editor.commit();
    }

    /**
     * function gets password from shared preferences file
     * @param context application context
     * @return User account data
     */
    public static String getPassword(Context context) {
        sharedPreferences = context.getSharedPreferences("UserPassword", context.MODE_PRIVATE);
        return sharedPreferences.getString("Password", "");
    }
}
