package com.example.trivago.support_classes;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import com.example.trivago.ui.home.Home;

import org.json.JSONObject;

import java.util.LinkedList;

/**
 * class storing messages from aws
 */
public final class Messages {

    private static String message = "";

    private Messages() {};

    public static String getMessage() {
        return message;
    }

    public static void setMessage(String value) {
        message = value;
    }

    /**
     * function saving message from json response
     * @param json
     */
    public static void saveMessage(JSONObject json, String usage, Context context) {
        setMessage("Good parameters");
        try {
            System.out.println(usage + " " + json.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if(json.has("Message")) {
                setMessage(json.get("Message").toString());
            } else {
                setMessage(json.get("message").toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(message.equals("Invalid token")) {
            invalidTokenMessage(context);
        }
        System.out.println("Message: " + message);
    }

    /**
     * function sending credentials for new token after it expires
     * @param context application context
     */
    public static void invalidTokenMessage(final Context context) {
        LinkedList<String> list = new LinkedList<String>();
        list.add(SharedPreferencesManager.getEmail(context));
        list.add(SharedPreferencesManager.getPassword(context));
        // clearing data so application is not sending json with invalid authorization token
        SharedPreferencesManager.clearUserData(context);

        JSONObject json = JSONManager.createJSON("SignIn", list);
        JSONManager.sendJSONObject(null, context, "SignIn", json);
    }

}
