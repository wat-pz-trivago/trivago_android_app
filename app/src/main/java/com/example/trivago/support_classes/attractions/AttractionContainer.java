package com.example.trivago.support_classes.attractions;

/**
 * class for storing values of single attraction
 */
public class AttractionContainer {

    private String attractionId;
    private String objectId;
    private String name;
    private String price;
    private String availableFrom;
    private String availableTo;
    private String description;
    private String duration;
    private String breakBeetween;

    public AttractionContainer() {
        attractionId = null;
        objectId = null;
        name = null;
        price = null;
        availableFrom = null;
        availableTo = null;
        description = null;
        duration = null;
        breakBeetween = null;
    }

    public String getAttractionId() {
        return attractionId;
    }

    public void setAttractionId(String attractionId) {
        this.attractionId = attractionId;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAvailableFrom() {
        return availableFrom;
    }

    public void setAvailableFrom(String availableFrom) {
        this.availableFrom = availableFrom;
    }

    public String getAvailableTo() {
        return availableTo;
    }

    public void setAvailableTo(String availableTo) {
        this.availableTo = availableTo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getBreakBeetween() {
        return breakBeetween;
    }

    public void setBreakBeetween(String breakBeetween) {
        this.breakBeetween = breakBeetween;
    }
}
