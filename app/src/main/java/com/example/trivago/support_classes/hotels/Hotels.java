package com.example.trivago.support_classes.hotels;

import com.example.trivago.support_classes.hotels.HotelContainer;
import com.example.trivago.support_classes.rooms.Rooms;
import com.example.trivago.ui.home.Home;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Support class for storing information about hotels
 */
public final class Hotels {

    private static LinkedList<HotelContainer> hotel = new LinkedList<HotelContainer>();
    private static String hotelNameToShow;

    private Hotels() {}

    public static LinkedList<HotelContainer> getAllHotels(){
        return hotel;
    }

    public static HotelContainer getHotel(int index) {
        return hotel.get(index);
    }

    public static void saveHotels(JSONObject json) {
        hotel.clear();
        try {
            JSONArray objects = json.getJSONArray("Objects");

            for(int i = 0; i < objects.length(); i++) {
                JSONObject currentObject = objects.getJSONObject(i);

                HotelContainer hotelContainer = new HotelContainer();
                hotelContainer.setObjectId(currentObject.get("ObjectId").toString());
                hotelContainer.setName(currentObject.getString("ObjectName"));
                hotelContainer.setCountry(currentObject.getString("Country"));
                hotelContainer.setCity(currentObject.getString("City"));
                hotelContainer.setStreet(currentObject.getString("Street"));
                hotelContainer.setHouseNumber(currentObject.get("HouseNumber").toString());
                hotelContainer.setZipCode(currentObject.getString("PostalCode"));
                hotelContainer.setStandard(currentObject.getString("Standard"));
                hotelContainer.setDescription(currentObject.getString("Description"));

                hotel.add(hotelContainer);
            }

        } catch(Exception e) {
            e.printStackTrace();
        }

        Rooms.clearRooms();

    }

    public static void setHotelToShow(String hotelName) {
        hotelNameToShow = hotelName;
    }

    public static String getHotelNameToShow() {
        return hotelNameToShow;
    }
}
