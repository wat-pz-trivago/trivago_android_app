package com.example.trivago.support_classes.filters;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public final class SearchFilters {

    // collection containing all countries from database
    private static Set<String> countries = new HashSet<String>();
    // collection containin all hotel entries from database
    private static LinkedList<String>[] allHotels = new LinkedList[3];

    private static String from;
    private static String to;
    private static String persons;
    private static String country;
    private static String city;
    private static String hotel;
    private static boolean bRoom, bHotel, bAttraction;

    private SearchFilters() { }

    /**
     * function adds country to the set
     * @param countryName
     */
    public static void addCountrySet(String countryName) {
        countries.add(countryName);
    }

    /**
     * function returns stored set of countries
     * @return
     */
    public static Set<String> getCountrySet() {
        return countries;
    }

    /**
     * function adds hotel to the list
     * @param arrayIndex value of the index should be 0(country), 1(city), 2(hotel)
     * @param name name added to the list
     */
    public static void addAllHotels(int arrayIndex, String name) {
        if(allHotels[arrayIndex] == null) {
            allHotels[arrayIndex] = new LinkedList<String>();
        }
        allHotels[arrayIndex].add(name);
    }

    /**
     * function returns array of lists containing all hotels
     * @return
     */
    public static LinkedList<String>[] getAllHotels() {
        return allHotels;
    }

    public static void saveFilters(JSONObject response) {
        clearValues();

        JSONObject objectCountries = response;
        JSONArray keysCountries = objectCountries.names();

        for (int i = 0; i < keysCountries.length(); i++) {
            try {

                String sCountry = keysCountries.getString (i); // Here's your key
                SearchFilters.addCountrySet(sCountry);

                JSONObject objectCities = objectCountries.getJSONObject(sCountry);
                JSONArray keysCities = objectCities.names();

                // loop for cities
                for (int j = 0; j < keysCities.length(); j++) {
                    try {
                        String sCity = keysCities.getString(j); // Here's your key
                        JSONArray arrayHotels = objectCities.getJSONArray(sCity);

                        // loop for hotels
                        for (int k = 0; k < arrayHotels.length(); k++) {
                            try {
                                String sHotel = arrayHotels.getString(k);

                                SearchFilters.addAllHotels(0, sCountry);
                                SearchFilters.addAllHotels(1, sCity);
                                SearchFilters.addAllHotels(2, sHotel);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch(Exception e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * getter for "from" variable
     * @return
     */
    public static String getFrom() {
        return from;
    }

    /**
     * setter for "from" variable
     * @param from
     */
    public static void setFrom(String from) {
        SearchFilters.from = from;
    }

    /**
     * getter for "to" variable
     * @return
     */
    public static String getTo() {
        return to;
    }

    /**
     * setter for "to" variable
     * @param to
     */
    public static void setTo(String to) {
        SearchFilters.to = to;
    }

    /**
     * getter for "persons" variable
     * @return
     */
    public static String getPersons() {
        return persons;
    }

    /**
     * setter for "persons" variable
     * @param persons
     */
    public static void setPersons(String persons) {
        SearchFilters.persons = persons;
    }

    /**
     * getter for "country" variable
     * @return
     */
    public static String getCountry() {
        return country;
    }

    /**
     * setter for "country" variable
     * @param country
     */
    public static void setCountry(String country) {
        SearchFilters.country = country;
    }

    /**
     * getter for "city" variable
     * @return
     */
    public static String getCity() {
        return city;
    }

    /**
     * setter for "city" variable
     * @param city
     */
    public static void setCity(String city) {
        SearchFilters.city = city;
    }

    /**
     * getter for "hotel" variable
     * @return
     */
    public static String getHotel() {
        return hotel;
    }

    /**
     * setter for "hotel" variable
     * @param hotel
     */
    public static void setHotel(String hotel) {
        SearchFilters.hotel = hotel;
    }

    /**
     * function for clearing all variables
     */
    public static void clearValues() {
        from = null;
        to = null;
        persons = null;
        country = null;
        city = null;
        hotel = null;
        countries.clear();

        for(int i = 0; i < 3; i++) {
            if(allHotels[i] != null) {
                allHotels[i].clear();
            }
        }
    }

    public static boolean isbRoom() {
        return bRoom;
    }

    public static void setbRoom(boolean bRoom) {
        SearchFilters.bRoom = bRoom;
    }

    public static boolean isbHotel() {
        return bHotel;
    }

    public static void setbHotel(boolean bHotel) {
        SearchFilters.bHotel = bHotel;
    }

    public static boolean isbAttraction() {
        return bAttraction;
    }

    public static void setbAttraction(boolean bAttraction) {
        SearchFilters.bAttraction = bAttraction;
    }
}
