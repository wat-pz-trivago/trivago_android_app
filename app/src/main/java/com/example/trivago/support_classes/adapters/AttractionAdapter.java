package com.example.trivago.support_classes.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.trivago.R;
import com.example.trivago.support_classes.JSONManager;
import com.example.trivago.support_classes.attractions.AttractionContainer;
import com.example.trivago.support_classes.filters.ReservationFilters;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;

public class AttractionAdapter extends ArrayAdapter<AttractionContainer> {

    private Context context;
    private Activity activity;
    private LinkedList<String> list = new LinkedList<String>();

    public AttractionAdapter(Context context, ArrayList<AttractionContainer> attractions, Activity activity) {
        super(context, 0, attractions);
        this.context = context;
        this.activity = activity;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final AttractionContainer attractionContainer = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_attraction, parent, false);
        }
        // Lookup view for data population
        TextView tvAttractionName = (TextView) convertView.findViewById(R.id.tvAttractionName);
        TextView tvAttractionPrice = (TextView) convertView.findViewById(R.id.tvAttractionPrice);
        TextView tvAttractionFrom = (TextView) convertView.findViewById(R.id.tvAttractionAvailableFrom);
        TextView tvAttractionTo = (TextView) convertView.findViewById(R.id.tvAttractionAvailableTo);
        TextView tvAttractionDescription = (TextView) convertView.findViewById(R.id.tvAttractionDescription);
        TextView tvAttractionDuration = (TextView) convertView.findViewById(R.id.tvAttractionDuration);
        TextView tvAttractionBreak = (TextView) convertView.findViewById(R.id.tvAttractionBreak);
        Button bAttractionSelect = (Button) convertView.findViewById(R.id.bAttractionSelect);

        // Populate the data into the template view using the data object
        tvAttractionName.setText(attractionContainer.getName());
        tvAttractionPrice.setText(attractionContainer.getPrice());
        tvAttractionFrom.setText(attractionContainer.getAvailableFrom());
        tvAttractionTo.setText(attractionContainer.getAvailableTo());
        tvAttractionDescription.setText(attractionContainer.getDescription());
        tvAttractionDuration.setText(attractionContainer.getDuration());
        tvAttractionBreak.setText(attractionContainer.getBreakBeetween());

        // Return the completed view to render on screen

        list.add(attractionContainer.getAttractionId());

        bAttractionSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAttractionDates(position);
            }
        });

        return convertView;
    }

    public void getAttractionDates(int position) {
        LinkedList<String> passList = new LinkedList<String>();
        passList.add(list.get(position));
        passList.add(ReservationFilters.getFrom());
        passList.add(ReservationFilters.getTo());
        JSONObject json = JSONManager.createJSON("GetAttractionDates", passList);
        JSONManager.sendJSONObject(null, context, "GetAttractionDates", json);
    }
}
