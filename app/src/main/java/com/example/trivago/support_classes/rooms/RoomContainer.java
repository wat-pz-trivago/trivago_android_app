package com.example.trivago.support_classes.rooms;

import java.util.LinkedList;

/**
 * class storing values for one room
 */
public class RoomContainer {

    private String roomId;
    private String objectId;
    private String roomNumber;
    private String price;
    private String bedConfiguration;
    private String guestsAmount;
    private String description;


    public RoomContainer() {
        roomId = null;
        objectId = null;
        roomNumber = null;
        price = null;
        bedConfiguration = null;
        guestsAmount = null;
        description = null;

    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBedConfiguration() {
        return bedConfiguration;
    }

    public void setBedConfiguration(String bedConfiguration) {
        this.bedConfiguration = bedConfiguration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getGuestsAmount() {
        return guestsAmount;
    }

    public void setGuestsAmount(String guestsAmount) {
        this.guestsAmount = guestsAmount;
    }
}
