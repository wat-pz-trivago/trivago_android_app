package com.example.trivago.support_classes.attractions;

import com.example.trivago.support_classes.hotels.HotelContainer;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;

/**
 * class for storing information about all attractions
 */
public final class Attractions {

    private static LinkedList<AttractionContainer> attraction = new LinkedList<AttractionContainer>();

    public static LinkedList<AttractionContainer> getAllAttractions(){
        return attraction;
    }

    public static AttractionContainer getAttraction(int index) {
        return attraction.get(index);
    }

    public static void saveAllAttractions(JSONObject json) {
        attraction.clear();
        try {
            JSONArray objects = json.getJSONArray("Attractions");
            for(int i = 0; i < objects.length(); i++) {
                JSONObject currentObject = objects.getJSONObject(i);

                AttractionContainer attractionContainer = new AttractionContainer();
                attractionContainer.setAttractionId(currentObject.get("AttractionId").toString());
                attractionContainer.setObjectId(currentObject.get("ObjectId").toString());
                attractionContainer.setName(currentObject.get("Name").toString());
                attractionContainer.setPrice(currentObject.get("Price").toString());
                attractionContainer.setAvailableFrom(currentObject.get("AvailableFrom").toString());
                attractionContainer.setAvailableTo(currentObject.get("AvailableTo").toString());
                attractionContainer.setDescription(currentObject.get("Description").toString());
                attractionContainer.setDuration(currentObject.get("Duration").toString());
                attractionContainer.setBreakBeetween(currentObject.get("Break").toString());

                attraction.add(attractionContainer);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
