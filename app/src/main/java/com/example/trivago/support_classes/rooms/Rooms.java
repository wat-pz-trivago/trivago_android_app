package com.example.trivago.support_classes.rooms;

import com.example.trivago.support_classes.hotels.HotelContainer;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Support class for storing informations about rooms
 */
public final class Rooms {

    private static LinkedList<RoomContainer> room = new LinkedList<RoomContainer>();

    private static boolean roomsFound = false;

    private Rooms() {}

    public static LinkedList<RoomContainer> getAllRooms(){
        return room;
    }

    public static RoomContainer getRoom(int index) {
        return room.get(index);
    }

    public static void saveRooms(JSONObject json) {
        room.clear();
        try {
            JSONArray objects = json.getJSONArray("Rooms");

            for(int i = 0; i < objects.length(); i++) {
                JSONObject currentObject = objects.getJSONObject(i);

                RoomContainer roomContainer = new RoomContainer();
                roomContainer.setRoomId(currentObject.get("RoomId").toString());
                roomContainer.setObjectId(currentObject.get("ObjectId").toString());
                roomContainer.setRoomNumber(currentObject.get("RoomNumber").toString());
                roomContainer.setPrice(currentObject.get("Price").toString());
                roomContainer.setBedConfiguration(currentObject.getString("BedConfiguration"));
                roomContainer.setGuestsAmount(currentObject.get("GuestsAmount").toString());
                roomContainer.setDescription(currentObject.getString("Description"));


                room.add(roomContainer);
                roomsFound = true;
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void clearRooms() {
        room.clear();
        roomsFound = false;
    }

    public static boolean isRoomsFound() {
        return roomsFound;
    }
}
