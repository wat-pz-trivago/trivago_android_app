package com.example.trivago.ui.search_results;

import android.os.Bundle;
import android.widget.ListView;

import com.example.trivago.R;
import com.example.trivago.support_classes.adapters.AttractionAdapter;
import com.example.trivago.support_classes.adapters.HotelAdapter;
import com.example.trivago.support_classes.adapters.RoomAdapter;
import com.example.trivago.support_classes.attractions.AttractionContainer;
import com.example.trivago.support_classes.attractions.Attractions;
import com.example.trivago.support_classes.filters.SearchFilters;
import com.example.trivago.support_classes.hotels.HotelContainer;
import com.example.trivago.support_classes.hotels.Hotels;
import com.example.trivago.support_classes.rooms.RoomContainer;
import com.example.trivago.support_classes.rooms.Rooms;
import com.example.trivago.ui.home.Home;

import java.util.ArrayList;

public class SearchResults extends Home {

    ListView lvSearchResults;
    ArrayList<HotelContainer> arrayHotels;
    ArrayList<RoomContainer> arrayRooms;
    ArrayList<AttractionContainer> arrayAttractions;
    HotelAdapter adapterHotels;
    RoomAdapter adapterRooms;
    AttractionAdapter adapterAttractions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        drawerLayout = findViewById(R.id.drawer_layout);
        setMenuItems();


        lvSearchResults = (ListView) findViewById(R.id.lvSearchResults);

        setListView();

    }

    public void setListView() {
        arrayHotels = new ArrayList<HotelContainer>();
        arrayRooms = new ArrayList<RoomContainer>();
        arrayAttractions = new ArrayList<AttractionContainer>();

        for(int i = 0; i < Rooms.getAllRooms().size(); i++) {
            arrayRooms.add(Rooms.getRoom(i));
        }

        for(int i = 0; i < Hotels.getAllHotels().size(); i++) {
            arrayHotels.add(Hotels.getHotel(i));
        }

        for(int i = 0; i < Attractions.getAllAttractions().size(); i++ ) {
            arrayAttractions.add(Attractions.getAttraction(i));
        }

        adapterHotels = new HotelAdapter(this, arrayHotels, this);
        adapterRooms = new RoomAdapter(this, arrayRooms, this);
        adapterAttractions = new AttractionAdapter(this, arrayAttractions, this);

        if (SearchFilters.isbRoom()) {
            lvSearchResults.setAdapter(adapterRooms);
        } else if (SearchFilters.isbHotel()){
            lvSearchResults.setAdapter(adapterHotels);
        } else if (SearchFilters.isbAttraction()) {
            lvSearchResults.setAdapter(adapterAttractions);
        }

    }

}