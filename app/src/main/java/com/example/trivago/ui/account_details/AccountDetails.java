package com.example.trivago.ui.account_details;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.trivago.R;
import com.example.trivago.support_classes.SharedPreferencesManager;
import com.example.trivago.ui.home.Home;
import com.example.trivago.ui.registration.RegisterUser;

/**
 * Show and let edit AccountDetails
 */
public class AccountDetails extends Home {

    EditText etFirstName, etLastName, etPhoneNum, etBirthDate, etCountry, etCity, etPostalCode, etSex, etBusinessRole;
    Button btAccept, btCancel;
    //    String sFirstName, sLastName, sPhoneNum, sBirthDate, sCountry, sCity, sPostalCode, sSex, sBusinessRole;
    Button btEdit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_details);

        drawerLayout = findViewById(R.id.drawer_layout);
        setMenuItems();

        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etPhoneNum = (EditText) findViewById(R.id.etPhoneNum);
        etBirthDate = (EditText) findViewById(R.id.etBirthDate);
        etCountry = (EditText) findViewById(R.id.etCountry);
        etCity = (EditText) findViewById(R.id.etCity);
        etPostalCode = (EditText) findViewById(R.id.etPostalCode);
        etSex = (EditText) findViewById(R.id.etSex);
        btEdit = (Button) findViewById(R.id.btEdit);
        btAccept = (Button) findViewById(R.id.btAccept);
        btCancel = (Button) findViewById(R.id.btCancel);
        //etBusinessRole = (EditText) findViewById(R.id.etBusinessRole);

        setTextViewValues();
        btEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onEdit();
            }
        });
        btAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAccept();
            }
        });
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCancel();
            }
        });
    }

    public void setTextViewValues() {
        SharedPreferencesManager spManager = new SharedPreferencesManager();
        String sFirstName = spManager.getFirstName(this);
        String sLastName = spManager.getLastName(this);
        String sTelephoneNumber = spManager.getPhoneNumber(this);
        String sPostCode = spManager.getPostalCode(this);
        String sSex = spManager.getPostalCode(this);
        String sCity = spManager.getCity(this);
        String sCountry = spManager.getCountry(this);
        String sBirthDate = spManager.getBirthDate(this);
        if(!sFirstName.isEmpty())
            etFirstName.setText(spManager.getFirstName(getApplicationContext()));
        if(!sLastName.isEmpty())
            etLastName.setText(spManager.getLastName(getApplicationContext()));
        if(!sTelephoneNumber.isEmpty())
            etPhoneNum.setText(spManager.getPhoneNumber(getApplicationContext()));
        if(!sBirthDate.isEmpty())
            etBirthDate.setText(spManager.getBirthDate(getApplicationContext()));
        if(!sCountry.isEmpty())
            etCountry.setText(spManager.getCountry(getApplicationContext()));
        if(!sCity.isEmpty())
            etCity.setText(spManager.getCity(getApplicationContext()));
        if(!sPostCode.isEmpty()||!(sPostCode =="null")||!(sPostCode==null))
            etPostalCode.setText(spManager.getPostalCode(getApplicationContext()));
        if(!sSex.isEmpty())
            etSex.setText(spManager.getSex(getApplicationContext()));
        // etBusinessRole.setText(spManager.getBusinessRole(getApplicationContext()));

    }
    public void onEdit(){
        etAllow(etFirstName);
        etAllow(etLastName);
        etAllow(etPhoneNum);
        etAllow(etBirthDate);
        etAllow(etCountry);
        etAllow(etCity);
        etAllow(etPostalCode);
        etAllow(etSex);
        btCancel.setVisibility(View.VISIBLE);
        btAccept.setVisibility(View.VISIBLE);
        btEdit.setVisibility(View.GONE);
    }

    protected void etAllow(EditText editText){
        editText.setClickable(true);
        editText.setFocusable(true);
        editText.setCursorVisible(true);
        editText.setEnabled(true);

    }
    protected void onAccept(){


    }
    protected void onCancel(){
        Intent intent = getIntent();
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
        overridePendingTransition(0, 0);

    }
}
