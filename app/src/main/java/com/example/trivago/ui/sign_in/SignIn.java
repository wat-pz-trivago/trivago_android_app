package com.example.trivago.ui.sign_in;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.trivago.support_classes.JSONManager;
import com.example.trivago.support_classes.Messages;
import com.example.trivago.support_classes.SharedPreferencesManager;
import com.example.trivago.ui.account_details.AccountDetails;
import com.example.trivago.ui.home.Home;
import com.example.trivago.R;
import com.example.trivago.ui.registration.RegisterUser;
import com.example.trivago.ui.registration.RegisterUserDetails;
import com.example.trivago.ui.search_results.SearchResults;

import org.json.JSONObject;

import java.util.LinkedList;

public class SignIn extends Home {

    EditText etEmail, etPassword;
    String sEmail, sPassword;
    Button bConfirm;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        drawerLayout = findViewById(R.id.drawer_layout);
        setMenuItems();

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        bConfirm = (Button) findViewById(R.id.bConfirm);
        textView = (TextView) findViewById(R.id.textView);

        bConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCredentials();
            }
        });

    }

    /**
     * function sends request with user email and password to sign in to personal account
     */
    public void sendCredentials() {
        LinkedList<String> list = new LinkedList<String>();
        sEmail = etEmail.getText().toString();
        sPassword = etPassword.getText().toString();

        SharedPreferencesManager.saveEmail(this.getApplicationContext(), sEmail);
        SharedPreferencesManager.savePassword(this.getApplicationContext(), sPassword);

        list.add(sEmail);
        list.add(sPassword);

        JSONObject json = JSONManager.createJSON("SignIn", list);
        JSONManager.sendJSONObject(null, this.getApplicationContext(), "SignIn", json);

        final Activity activity = this;

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // Actions to do after 1 second
                if(Messages.getMessage().equals("Good parameters")) {
       //             getAllReservations();
                    redirectActivity(getApplicationContext(), activity, Home.class);
                } else {// show message error
                    AlertDialog alertDialog = new AlertDialog.Builder(SignIn.this).create();
                    alertDialog.setMessage(Messages.getMessage());
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();

                                }
                            });
                    alertDialog.show();
                }
            }
        }, 1000);



    }



}
