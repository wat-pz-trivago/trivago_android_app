package com.example.trivago.ui.registration;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.trivago.R;
import com.example.trivago.support_classes.JSONManager;
import com.example.trivago.ui.home.Home;


import org.json.JSONObject;

import java.util.LinkedList;


public class RegisterUserDetails extends AppCompatActivity {

    private String sEmail, sPasswd, sFirstName, sLastName, sSex, sPhoneNum, sBirthDate, sCity, sPostCode, sCountry;
    private String sRegistrationType, sEmployeeType;
    private EditText etFirstName, etLastName, etPhoneNum, etBirthDate, etCountry, etCity, etPostCode;
    private RadioButton rbMale, rbFemale;
    private Button bConfirm;
    private JSONObject json;
    private TextView textView;
    private RadioGroup rgSex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user_details);

        initStrings();

        textView = (TextView) findViewById(R.id.textView);

        sEmail = getIntent().getStringExtra("Email");
        sPasswd = getIntent().getStringExtra("Password");
        sRegistrationType = getIntent().getStringExtra("RegistrationType");

        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etPhoneNum = (EditText) findViewById(R.id.etPhoneNum);
        etBirthDate = (EditText) findViewById(R.id.etBirthDate);
        etCountry = (EditText) findViewById(R.id.bCountry);
        etCity = (EditText) findViewById(R.id.bCity);
        etPostCode = (EditText) findViewById(R.id.etPostCode);

        rgSex = (RadioGroup) findViewById(R.id.rgSex);
        rbMale = (RadioButton) findViewById(R.id.rbMale);
        rbFemale = (RadioButton) findViewById(R.id.rbFemale);
        bConfirm = (Button) findViewById(R.id.bConfirm);

        if(sRegistrationType.equals("Client")) {
        } else {
            sEmployeeType = getIntent().getStringExtra("EmployeeType");
        }

        rbFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sSex = "Female";
            }
        });
        rbMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sSex = "Male";
            }
        });

        bConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkEditText();
            }
        });
    }

    private void checkEditText() {
        // First name regex pattern
        String ptrName = "^[A-Z]{1}[a-z]+$";
        // Phone number regex pattern
        String ptrPhoneNum = "^[1-9]{1}[0-9]{8}$";
        /* Birthdate regex pattern
         *  date format dd.mm.yyyy*/
        String ptrBirthDate = "^\\s*(3[01]|[12][0-9]|0?[1-9])\\.(1[012]|0?[1-9])\\.((?:19|20)\\d{2})\\s*$";
        // City regex pattern.
        String ptrCity ="^[A-z]+\\s{0,1}[A-z]+$";
        // Postal code regex pattern
        String ptrPostCode = "^[0-9]{2}[-]{1}[0-9]{3}$";
        // Country regex pattern
        String ptrCountry = "[a-zA-Z]{2,}";

        sFirstName = etFirstName.getText().toString();
        sLastName = etLastName.getText().toString();
        sPhoneNum = etPhoneNum.getText().toString();
        sBirthDate = etBirthDate.getText().toString();
        sCity = etCity.getText().toString();
        sPostCode = etPostCode.getText().toString();
        sCountry = etCountry.getText().toString();

        final boolean goodFirstName = sFirstName.matches(ptrName);
        final boolean goodLastName = sLastName.matches(ptrName);
        final boolean goodPhoneNum = sPhoneNum.matches(ptrPhoneNum);
        final boolean goodBirthDate = sBirthDate.matches(ptrBirthDate);
        final boolean goodCountry = sCountry.matches(ptrCountry);
        boolean goodCity = true;
        boolean goodPostCode = true;

        // checking if extra fields are empty
        if(!sCity.equals("")) {
            goodCity = sCity.matches(ptrCity);
        }
        if(!sPostCode.equals("")) {
            goodPostCode = sPostCode.matches(ptrPostCode);
        }

        final boolean goodSex = !sSex.isEmpty();
        final boolean finalGoodCity = goodCity;
        final boolean finalGoodPostCode = goodPostCode;

        final AlertDialog alertDialog = new AlertDialog.Builder(RegisterUserDetails.this).create();
        alertDialog.setTitle("Message");
        if(goodFirstName && goodLastName && goodPhoneNum && goodCountry
                && goodBirthDate && finalGoodCity && goodSex && finalGoodPostCode) {
            alertDialog.setMessage("Good credentials");
        } else {
            alertDialog.setMessage("Bad credentials.");
            if(!goodFirstName) { etFirstName.setBackgroundColor(Color.RED); }
            else { etFirstName.setBackgroundColor(Color.GREEN); }

            if(!goodLastName) { etLastName.setBackgroundColor(Color.RED); }
            else { etLastName.setBackgroundColor(Color.GREEN); }

            if(!goodPhoneNum) { etPhoneNum.setBackgroundColor(Color.RED); }
            else { etPhoneNum.setBackgroundColor(Color.GREEN); }

            if(!goodBirthDate) { etBirthDate.setBackgroundColor(Color.RED); }
            else { etBirthDate.setBackgroundColor(Color.GREEN); }

            if(!goodCountry) { etCountry.setBackgroundColor(Color.RED); }
            else { etCountry.setBackgroundColor(Color.GREEN); }

            if(!goodCity) { etCity.setBackgroundColor(Color.RED); }
            else { etCity.setBackgroundColor(Color.GREEN); }

            if(!goodPostCode) { etPostCode.setBackgroundColor(Color.RED); }
            else { etPostCode.setBackgroundColor(Color.GREEN); }

            if(!goodSex) { rgSex.setBackgroundColor(Color.RED); }
            else { rgSex.setBackgroundColor(Color.GREEN); }
        }

        final Activity activity = this;

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if(goodFirstName && goodLastName  && goodPhoneNum && goodCountry
                                && goodBirthDate && finalGoodCity && goodSex && finalGoodPostCode) {
                            LinkedList<String> list = new LinkedList<String>();
                            if(sRegistrationType.equals("Client")) {
                                list.add("Client");
                            } else {
                                list.add(sEmployeeType);
                            }
                            list.add(sEmail);
                            list.add(sPasswd);
                            list.add(sFirstName);
                            list.add(sLastName);
                            list.add(sSex);
                            list.add(sBirthDate);
                            list.add(sPhoneNum);
                            list.add(sCountry);
                            list.add(sCity);
                            list.add(sPostCode);

                            json = JSONManager.createJSON("Register", list);
                            JSONManager.sendJSONObject(textView, getApplicationContext(), "Register", json);
                //            textView.setText(jsonObject.toString());

                            Home.redirectActivity(getApplicationContext(), activity, Home.class);
                        }
                    }
                });
        alertDialog.show();

    }

    private void initStrings() {
        sEmail = sPasswd = sFirstName = sLastName = sSex = sPhoneNum = sBirthDate = sCity = sPostCode = sCountry = "";
        sRegistrationType = sEmployeeType = "";
    }

}
