package com.example.trivago.ui.registration;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.trivago.R;
import com.example.trivago.support_classes.JSONManager;
import com.example.trivago.support_classes.Messages;
import com.example.trivago.ui.account_details.AccountDetails;
import com.example.trivago.ui.home.Home;
import com.example.trivago.ui.sign_in.SignIn;

import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Class used for creating new user accounts, both customers and employees
 */
public class RegisterUser extends Home {

    //DrawerLayout drawerLayout;
    TextView textView, tvObligatoryRadioGroup;
    EditText etEmail, etPassword, etPasswordConfirm;
    String sRegistrationType,sEmployeeType;
    Button bConfirm;
    RadioButton rbEmployee, rbManager, rbDirector;

    /**
     * Function used when activity starts.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        sRegistrationType = getIntent().getStringExtra("RegistrationType");

        drawerLayout = findViewById(R.id.drawer_layout);
        setMenuItems();

        textView = (TextView) findViewById(R.id.textView);
        tvObligatoryRadioGroup = (TextView) findViewById(R.id.tvObligatoryRadioGroup);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etPasswordConfirm = (EditText) findViewById(R.id.etPasswordConfirm);
        rbEmployee = (RadioButton) findViewById(R.id.rbReceptionist);
        rbManager = (RadioButton) findViewById(R.id.rbManager);
        rbDirector = (RadioButton) findViewById(R.id.rbDirector);
        bConfirm = (Button) findViewById(R.id.bConfirm);

        // Hiding radio buttons if its Customer/Client registration type
        if(sRegistrationType.equals("Client")) {
            rbEmployee.setVisibility(View.GONE);
            rbManager.setVisibility(View.GONE);
            rbDirector.setVisibility(View.GONE);
            tvObligatoryRadioGroup.setText("");
        } else {
            textView.setText("Register new employee account");
        }
        // Setting events after clicking on radio button
        rbEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sEmployeeType = "Receptionist";
            }
        });
        rbManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sEmployeeType = "Manager";
            }
        });
        rbDirector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sEmployeeType = "Director";
            }
        });

        bConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkEditText();
            }
        });

    }

    /**
     * Function checks if text written by user is proper
     */
    private void checkEditText() {
        // Email regex pattern
        String ptrEmail = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        /* Password regex pattern
         * a digit must occur at least once
         * a lower case letter must occur at least once
         * an upper case letter must occur at least once
         * a special character must occur at least once
         * no whitespace allowed in the entire string
         * at least 8 characters */
        String ptrPasswd = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_=+-])(?=\\S+$).{8,}";

        final String sEmail = etEmail.getText().toString();
        final String sPasswd = etPassword.getText().toString();
        String sPasswdConf = etPasswordConfirm.getText().toString();

        final boolean goodEmail = sEmail.matches(ptrEmail);
        final boolean goodPasswd = sPasswd.matches(ptrPasswd);
        final boolean matchPasswd = sPasswdConf.equals(sPasswd);
        boolean goodEmployeeType = true;

        if(sRegistrationType.equals("Employee") && sEmployeeType.isEmpty() == false) {
            goodEmployeeType = true;
        }

        final boolean finalGoodEmployeeType = goodEmployeeType;

        final AlertDialog alertDialog = new AlertDialog.Builder(RegisterUser.this).create();
        alertDialog.setTitle("Message");

     /*   if(goodEmail && goodPasswd && matchPasswd) {
            alertDialog.setMessage("Good credentials");
        } else {
            alertDialog.setMessage("Bad credentials");
        }*/
        // setting colors for written values
        if(goodEmail) {
            etEmail.setBackgroundColor(Color.GREEN);
        } else {
            etEmail.setBackgroundColor(Color.RED);
        }
        if(goodPasswd) {
            etPassword.setBackgroundColor(Color.GREEN);
        } else {
            etPassword.setBackgroundColor(Color.RED);
        }
        if(matchPasswd) {
            etPasswordConfirm.setBackgroundColor(Color.GREEN);
        } else {
            etPasswordConfirm.setBackgroundColor(Color.RED);
        }

        emailCheck();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // Actions to do after 1 second
                alertDialog.setMessage(Messages.getMessage());

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                if(goodEmail && goodPasswd && matchPasswd && finalGoodEmployeeType) {

                                    if(!Messages.getMessage().equals("Account with given e-mail exists")) {
                                        Intent intent = new Intent(RegisterUser.this, RegisterUserDetails.class);
                                        intent.putExtra("Email", sEmail);
                                        intent.putExtra("Password", sPasswd);
                                        intent.putExtra("RegistrationType", sRegistrationType);

                                        if(sRegistrationType.equals("Employee")) {
                                            intent.putExtra("EmployeeType", sEmployeeType);
                                        }

                                        startActivity(intent);
                                        finish();
                                    }

                                }
                            }
                        });
                alertDialog.show();
            }
        }, 1000);


    }

    /**
     * function sends json checking if account with same email exists
     */
    public void emailCheck() {
        LinkedList<String> list = new LinkedList<String>();
        list.add(etEmail.getText().toString());
        JSONObject json = JSONManager.createJSON("EmailCheck", list);
        JSONManager.sendJSONObject(null, this, "EmailCheck", json);
    }

}
