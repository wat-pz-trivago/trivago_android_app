package com.example.trivago.ui.reservations;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.trivago.R;
import com.example.trivago.support_classes.adapters.RoomReservationAdapter;
import com.example.trivago.support_classes.reservations.RoomReservationContainer;
import com.example.trivago.support_classes.reservations.Reservations;
import com.example.trivago.ui.home.Home;

import java.util.ArrayList;

public class ReservationsUI extends Home {

    TextView textView;
    ListView lvSearchResults;
    Button bRoomReservations, bAttractionReservations;
    ArrayList<RoomReservationContainer> arrayReservations;
    RoomReservationAdapter adapterReservation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservations);

        drawerLayout = findViewById(R.id.drawer_layout);
        setMenuItems();
        lvSearchResults = (ListView) findViewById(R.id.lvSearchResults);

        textView = (TextView) findViewById(R.id.textView);
        bRoomReservations = (Button) findViewById(R.id.bRoomReservations);
        bAttractionReservations = (Button) findViewById(R.id.bAttractionReservations);

        bRoomReservations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setListView("Rooms");
            }
        });

        bAttractionReservations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setListView("Attractions");
            }
        });
        // Actions to do after 1 second
    }

    public void setListView(String usage) {
        if(usage.equals("Rooms")) {
            arrayReservations = new ArrayList<RoomReservationContainer>();

            for(int i = 0; i < Reservations.getAllReservations().size(); i++) {
                arrayReservations.add(Reservations.getReservation(i));

            }
            adapterReservation = new RoomReservationAdapter(this, arrayReservations, this);
            lvSearchResults.setAdapter(adapterReservation);
        } else if (usage.equals("Attractions")) {

        }

    }

}