package com.example.trivago.ui.home;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.trivago.R;
import com.example.trivago.support_classes.JSONManager;
import com.example.trivago.support_classes.Messages;
import com.example.trivago.support_classes.filters.SearchFilters;
import com.example.trivago.support_classes.SharedPreferencesManager;
import com.example.trivago.ui.account_details.AccountDetails;
import com.example.trivago.ui.registration.RegisterUser;
import com.example.trivago.ui.reservations.ReservationsUI;
import com.example.trivago.ui.search_results.SearchResults;
import com.example.trivago.ui.sign_in.SignIn;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;

public class Home extends AppCompatActivity {
    //Initialize variable
    protected DrawerLayout drawerLayout;
    // variables used for showing and hiding menu items
    protected TextView tvHome, tvRegister, tvSignIn, tvAccountDetails, tvReservations;
    protected TextView tvLogout;
    TextView tvMessage;
    Button bCountry, bCity, bHotel, bDateOfStayFrom, bDateOfStayTo, bConfirm;
    EditText etNumOfPeople;
    String sCountry, sCity, sHotel,sNumOfPeople, sDateFrom, sDateTo, sRB;
    RadioButton rb1, rb2, rb3, rb4, rb5;
    RadioGroup rgStandard;
    int iFrom, iTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        createStrings();
        setMenuItems();
        tvMessage = (TextView) findViewById(R.id.tvMessage);

        drawerLayout = findViewById(R.id.drawer_layout);

        bCountry = (Button) findViewById(R.id.bCountry);
        bCity = (Button) findViewById(R.id.bCity);
        bHotel = (Button) findViewById(R.id.bHotel);
        bDateOfStayFrom = (Button) findViewById(R.id.bDateOfStayFrom);
        bDateOfStayTo = (Button) findViewById(R.id.bDateOfStayTo);
        rb1 = (RadioButton) findViewById(R.id.rb1);
        rb2 = (RadioButton) findViewById(R.id.rb2);
        rb3 = (RadioButton) findViewById(R.id.rb3);
        rb4 = (RadioButton) findViewById(R.id.rb4);
        rb5 = (RadioButton) findViewById(R.id.rb5);
        etNumOfPeople = (EditText) findViewById(R.id.etNumOfPeople);
        rgStandard = (RadioGroup) findViewById(R.id.rgStandard);
        bConfirm = (Button) findViewById(R.id.bConfirm);

        // setting on click listeners
        bCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countryPopUpMenu(v);
            }
        });
        bCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cityPopUpMenu(v);
            }
        });
        bHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hotelPopUpMenu(v);
            }
        });
        bDateOfStayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDateOfStay("From");
            }
        });
        bDateOfStayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDateOfStay("To");
            }
        });
        rb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sRB = "1";
            }
        });
        rb2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sRB = "2";
            }
        });
        rb3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sRB = "3";
            }
        });
        rb4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sRB = "4";
            }
        });
        rb5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sRB = "5";
            }
        });
        bConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFilters();
            }
        });


    }

    public void clickMenu(View view) {
        //Open drawer
        openDrawer(drawerLayout);
    }

    public static void openDrawer(DrawerLayout drawerLayout) {
        // Open drawer layout
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void clickLogo(View view) {
        // Show popup
        closeDrawer(drawerLayout);
    }

    public static void closeDrawer(DrawerLayout drawerLayout) {
        // Close drawer layout if open
        if(drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    public void clickHome(View view) {
        //Redirect activity
        redirectActivity(getApplicationContext(),this, Home.class);
    }

    public void clickRegister(View view) {
        // Redirect activity to dashboard
        redirectActivity(getApplicationContext(),this, RegisterUser.class);
    }

    public void clickSignIn(View view) {
        redirectActivity(getApplicationContext(), this, SignIn.class);
    }

    public void clickAccountDetails(View view) {
        redirectActivity(getApplicationContext(),this, AccountDetails.class);
    }

    public void clickReservations(View view) {
        getAllReservations();
        redirectActivity(getApplicationContext(),this, ReservationsUI.class);
    }

    public void clickLogout(View view) {
        JSONObject json = JSONManager.createJSON("Initial", null);
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager();
        sharedPreferencesManager.saveUserData(json, getApplicationContext());

        redirectActivity(getApplicationContext(), this, Home.class);
    }

    public static void redirectActivity(Context context, Activity activity, Class aClass) {
        Intent intent = new Intent(activity, aClass);
        // if button clicked is for user registration
        if(aClass.getSimpleName().equals("RegisterUser")) {
            SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager();

            String accountRole = sharedPreferencesManager.getBusinessRole(context);
            intent.putExtra("RegistrationType", accountRole);
        }

        if((activity.getClass().getSimpleName().equals("SearchResults") || activity.getClass().getSimpleName().equals("Home")
                || activity.getClass().getSimpleName().equals("ReservationsUI"))
                && aClass.getSimpleName().equals("SearchResults")) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        activity.startActivity(intent);

    }

    @Override
    protected void onPause() {
        super.onPause();
        closeDrawer(drawerLayout);
    }

    /**
     * function creates initializes Strings with "" value
     */
    public void createStrings() {
        sCountry = "";
        sCity = "";
        sHotel = "";
        sNumOfPeople = "";
        sDateFrom = "";
        sDateTo = "";
        sRB = "";
    }

    /**
     * function sets visibility of menu items from main_nav_drawer.xml
     */
    public void setMenuItems() {
        // general menu items
        tvHome = (TextView) findViewById(R.id.tvHome);
        tvRegister = (TextView) findViewById(R.id.tvRegister);
        tvSignIn = (TextView) findViewById(R.id.tvSignIn);
        tvAccountDetails = (TextView) findViewById(R.id.tvAccountDetails);
        tvReservations = (TextView) findViewById(R.id.tvReservations);
        // specific account menu items
        tvLogout = (TextView) findViewById(R.id.tvLogout);

        // user is not signed
        if(SharedPreferencesManager.getToken(this).equals("")) {
            tvAccountDetails.setVisibility(View.GONE);
            tvReservations.setVisibility(View.GONE);
            tvLogout.setVisibility(View.GONE);
        } else if (SharedPreferencesManager.getBusinessRole(getApplicationContext()).equals("Client")){ // customer is signed
            tvRegister.setVisibility(View.GONE);
            tvSignIn.setVisibility(View.GONE);
        } else { // hotel employee is signed
            tvSignIn.setVisibility(View.GONE);
            tvReservations.setVisibility(View.GONE);
        }
    }
    /**
     * Function creating popup menu for country filters
     * @param view
     */
    public void countryPopUpMenu(View view) {
        PopupMenu popupMenu = new PopupMenu(this, view);

        // adding option all
        popupMenu.getMenu().add("All");

        Set<String> countrySet = SearchFilters.getCountrySet();
        // adding options to menu
        for(String country : countrySet) {
            popupMenu.getMenu().add(country);
        }

        popupMenu.show();

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                bCountry.setText(item.getTitle());

                String sCountry = item.getTitle().toString();
                // if we choose all countries
                if(!sCountry.equals("All")) {
                    bCity.setText("All");
                    bHotel.setText("All");
                    rgStandard.setVisibility(View.VISIBLE);
                }

                return false;
            }
        });
    }

    /**
     * Function creating popup menu for city filters
     * @param view
     */
    public void cityPopUpMenu(View view) {
        PopupMenu popupMenu = new PopupMenu(this, view);

        final LinkedList<String>[] hotelsList = SearchFilters.getAllHotels();
        String sCountry = bCountry.getText().toString();
        Set<String> citiesList = new TreeSet<>();

        //adding option ALl
        popupMenu.getMenu().add("All");
        // adding options to menu
        if(sCountry.equals("All")) { // if country wasn't set
            for(int i = 0; i < hotelsList[0].size(); i++) {
                citiesList.add(hotelsList[1].get(i));
            }
        } else { // if user choose the country
            for(int i = 0; i < hotelsList[0].size(); i++) {
                if(hotelsList[0].get(i).equals(sCountry)) { // if country was selected
                    citiesList.add(hotelsList[1].get(i));
                }
            }

        }
        for(String c : citiesList) {
            popupMenu.getMenu().add(c);
        }

        popupMenu.show();

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                bCity.setText(item.getTitle());

                String sCity = item.getTitle().toString();
                if(!sCity.equals("All")) {
                    for(int i = 0; i < hotelsList[0].size(); i++) {
                        if(hotelsList[1].get(i).equals(sCity)) {
                            bCountry.setText(hotelsList[0].get(i));
                            break;
                        }
                    }
                } else {
                    bHotel.setText("All");
                    rgStandard.setVisibility(View.VISIBLE);
                }

                return false;
            }
        });
    }

    /**
     * Function creating popup menu for hotel filters
     * @param view
     */
    public void hotelPopUpMenu(View view) {
        PopupMenu popupMenu = new PopupMenu(this, view);

        final LinkedList<String>[] hotelsList = SearchFilters.getAllHotels();
        String sCountry = bCountry.getText().toString();
        String sCity = bCity.getText().toString();

        // adding option all
        popupMenu.getMenu().add("All");

        for(int i = 0; i < hotelsList[0].size(); i++) {
            if(!sCity.equals("All")) { // if only city was selected
                if(hotelsList[1].get(i).equals(sCity)) {
                    popupMenu.getMenu().add(hotelsList[2].get(i));
                }
            } else if(sCity.equals("All") && !sCountry.equals("All")) { // if only country was selected
                if(hotelsList[0].get(i).equals(sCountry)) {
                    popupMenu.getMenu().add(hotelsList[2].get(i));
                }
            } else { // if nothing was selected
                popupMenu.getMenu().add(hotelsList[2].get(i));
            }
        }

        popupMenu.show();

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                bHotel.setText(item.getTitle());

                String sHotel = item.getTitle().toString();
                if(!sHotel.equals("All")) {
                    for(int i = 0; i < hotelsList[0].size(); i++) {
                        if(hotelsList[2].get(i).equals(sHotel)) {
                            bCountry.setText(hotelsList[0].get(i));
                            bCity.setText(hotelsList[1].get(i));

                            sRB = "";

                            break;
                        }
                    }

                    // setting visibility of radio group standard
                    rgStandard.setVisibility(View.GONE);

                } else {
                    // setting visibility of radio group standard
                    rgStandard.setVisibility(View.VISIBLE);
                }

                return false;
            }
        });
    }

    /**
     * function to set date of stay
     */
    public void setDateOfStay(final String type) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
            {
                String sDay, sMonth, sYear;
                // setting values in format DD.MM.YYYY
                if(dayOfMonth < 10) { sDay = "0" + Integer.toString(dayOfMonth); }
                else { sDay = Integer.toString(dayOfMonth); }

                if(month + 1 < 10) { sMonth = "0" + Integer.toString(month + 1); }
                else { sMonth = Integer.toString(month + 1); }

                sYear = Integer.toString(year);

                String date = sDay + "-" + sMonth + "-" + sYear;

                if(type.equals("From")) {
                    bDateOfStayFrom.setText(date);

                    date = sYear + sMonth + sDay;
                    iFrom = Integer.parseInt(date);

                } else {
                    bDateOfStayTo.setText(date);

                    date = sYear + sMonth + sDay;
                    iTo = Integer.parseInt(date);
                }
            }
        }, year, month, day);
        datePickerDialog.show();

    }

    /**
     * Function that sends selected filters to API
     */
    public void sendFilters() {
        // getting string values from selected filters
        sCountry = bCountry.getText().toString();
        sCity = bCity.getText().toString();
        sHotel = bHotel.getText().toString();
        sDateFrom = bDateOfStayFrom.getText().toString();
        sDateTo = bDateOfStayTo.getText().toString();
        sNumOfPeople = etNumOfPeople.getText().toString();

        // saving filters for later use
        SearchFilters.setFrom(sDateFrom);
        SearchFilters.setTo(sDateTo);
        SearchFilters.setPersons(sNumOfPeople);

        LinkedList<String> list = new LinkedList<String>();
        list.add(sCountry);
        list.add(sCity);
        list.add(sHotel);
        list.add(sDateFrom);
        list.add(sDateTo);
        list.add(sNumOfPeople);
        list.add(sRB);

        JSONObject json = JSONManager.createJSON("SendFilters", list);
        JSONManager.sendJSONObject(null, this.getApplicationContext(), "SendFilters", json);

        System.out.println(json.toString());

        final Activity activity = this;

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                boolean bGoodParameters = true;

                if(iFrom >= iTo || Integer.parseInt(sNumOfPeople) <= 0 || sCountry.equals("All")) {
                    bGoodParameters = false;
                    Messages.setMessage("Change date values and/or number of people and/or country");
                }

                // Actions to do after 1 second
                if(bGoodParameters) {// chosen parameters are good search search for values
                    redirectActivity(getApplicationContext(), activity, SearchResults.class);
                } else { // show message
                    AlertDialog alertDialog = new AlertDialog.Builder(Home.this).create();
                    alertDialog.setMessage(Messages.getMessage());
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }
        }, 1000);

    }

    /**
     * function sends request for user reservations
     */
    public void getAllReservations() {
        getRoomReservations();
    }

    /**
     * function sends request for room reservetians
     */
    public void getRoomReservations() {
        JSONManager.sendJSONObject(null, getApplicationContext(), "GetRoomReservations", null);
    }

}
